# DDR instance of Airflow

## Description 
Repository contains:  
-  Scripts and configs for deploying Airflow: *docker-compose.yaml*, *.env*, *airflow.cfg*  
-  Data workflows: DAGs that realize needed functionalities like data harvesting and instrument 
log export for [DDRapp](https://gitlab.com/data-driven-research/ddrapp), as part of DDR ecosystem  
  
Other information is in [DDRapp wiki](https://gitlab.com/data-driven-research/ddrapp/-/wikis/home).


## Deployment
For deployment, you need to be familiar with Docker Compose or Kubernetes. For the Docker version, follow this 
detailed [guide](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html); 
for Kubernetes, follow this [one](https://airflow.apache.org/docs/helm-chart/stable/index.html). 
The modified version of Docker Compose deployment used in this project can be found in the source files as 
*docker-compose.yaml* with accompanied *.env* and *airflow.cfg*. This version can be used for single-node deployments 
after setting the necessary variables. Setting them is described as comments in the source code in
the *deploy* directory. All configuration needed to be set is its example value marked by <<>>. 
You need to generate and let sign the app certificate, and move self-signed certificate created in the process of 
deploying DDRapp (Elasticsearch in particular) that has to be in *certs/ca* directory at your server.

To deploy the application to your server, you must copy the *dags* directory and contents of the *deploy* directory 
to your target directory, with all the necessary information and paths set in *deploy/docker-compose.yaml*, 
*deploy/airflow.cfg*, and *deploy/.env*, and all the required certificates and keys moved to the *deploy/certs* 
directory. Then create separate user *airflow* (and set its UID in *.env*), make directories *logs* and *plugins*. 
Set user and group of *dags*, *logs*, and *plugins* to airflow:root and *certs* to root:root (with its contents as well).

Then run:  
``
docker compose --profile flower up --wait
``

## Development

For developing, testing and deploying DAGs, you don't need to rebuild any Docker image. You only need to paste modified 
files to *dags* directory and wait for the Airflow refresh. In case you need to update custom Airflow image by adding 
new Airflow provider or general Python package, run:  
``
./build-release/release.sh {hub} {target version}
``

The container registry is used to release on [Docker Hub](https://hub.docker.com/r/cxiomi/airflow) with the hub name 
**cxiomi**, where the images are uploaded for subsequent deployment. The application is developed so that the actual 
deployment can be done with only a change of configuration files in the *deploy* directory. In the case of modifying 
the code and building an image for your institution, you need to use your container repository.

## Authors
Jan Kočí - lead, architect  
Věnceslav Chumchal - developer  
Martin Vích Vlasák - consultant  
Jakub Zach - tester, consultant  

## License
Apache-2.0

