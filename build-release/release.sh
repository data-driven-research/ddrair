#!/bin/bash

HUB=$1
VERSION=$2
IMAGE_NAME=$HUB/airflow

docker build -t "$IMAGE_NAME:$VERSION" .

if [ "$VERSION" != "test" ]; then
    docker push "$IMAGE_NAME:$VERSION"
fi
