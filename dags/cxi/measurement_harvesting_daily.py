import logging
from datetime import datetime

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.samba.hooks.samba import SambaHook
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.utils.dates import days_ago, timedelta
from airflow.operators.email import EmailOperator
from os import path

from pandas import Series

from helpers.samba import traverse_samba_directory
from helpers.sftp import recursive_list_directory
from helpers.cesnet_s3 import save_file_to_s3, get_object_on_s3
from helpers.elastic import get_measurement_metadata_by_instrument
from helpers.postgres import get_installed_instruments
from helpers.metadata import code_to_ascii

dag_args = {
    "owner": "cxi_tul",
    "start_date": days_ago(1),
    "schedule_interval": "0 11 * * *",
    "email_on_failure": True,
    "email": ["jakub.zach@tul.cz", "ddr@tul.cz"]
}


def harvest_from_windows(samba_conn_id: str, metadata: Series):
    bucket_name = metadata["resource_data_path"].removesuffix("/")
    data_path = metadata["measurement_data_path"]
    key_prefix = data_path.removeprefix(f"{bucket_name}/")

    try:
        with SambaHook(samba_conn_id=samba_conn_id) as samba_hook:
            logging.info(f"Copying files from relpath {data_path}")
            try:
                file_list = traverse_samba_directory(samba_hook, data_path)
            except FileNotFoundError:
                return

            for file_path in file_list:
                s3_key = path.join(key_prefix, file_path.replace(data_path, ""))
                attributes = samba_hook.stat(file_path)
                modified_time = attributes.st_mtime
                modified_datetime = datetime.fromtimestamp(modified_time)
                try:
                    s3_object = get_object_on_s3(bucket_name, s3_key)
                    s3_modified_datetime = s3_object.last_modified.replace(tzinfo=None)
                    if modified_datetime > s3_modified_datetime:
                        # Upload the newer file to S3
                        with samba_hook.open_file(file_path, "rb") as body:
                            if not save_file_to_s3(bucket_name, s3_key, body, code_to_ascii(metadata.to_dict())):
                                return False
                except FileNotFoundError:
                    # Upload the file to S3 if it doesn't exist
                    with samba_hook.open_file(file_path, "rb") as body:
                        if not save_file_to_s3(bucket_name, s3_key, body, code_to_ascii(metadata.to_dict())):
                            return False
    except Exception as e:
        send_email_task = EmailOperator(
            task_id='send_email_on_exception',
            to=dag_args["email"],
            subject='Exception in harvest_from_windows function',
            html_content=f'<p>An error occurred in the harvest_from_windows function: {str(e)}</p>',
            dag=dag,
        )
        send_email_task.execute(context=None)
        logging.error(f"Exception occurred: {str(e)}")

    return True


def harvest_from_linux(sftp_conn_id: str, metadata: Series):
    bucket_name = metadata["resource_data_path"].removesuffix("/")
    data_path = metadata["measurement_data_path"]
    key_prefix = data_path.removeprefix(f"{bucket_name}/")
    root_dir = metadata["instrument_custom_metadata"]["root_directory"]

    sftp_hook = SFTPHook(ssh_conn_id=sftp_conn_id)
    try:
        with sftp_hook.get_conn() as sftp_conn:
            logging.info(f"Going to directory {root_dir}")
            sftp_conn.chdir(root_dir)
            logging.info(f"Copying files from relpath {data_path}")
            try:
                file_list = recursive_list_directory(sftp_conn, data_path)
            except FileNotFoundError:
                return

            for file_path in file_list:
                s3_key = path.join(key_prefix, file_path.replace(data_path, ""))
                attributes = sftp_conn.stat(file_path)
                modified_time = attributes.st_mtime
                modified_datetime = datetime.fromtimestamp(modified_time)
                try:
                    s3_object = get_object_on_s3(bucket_name, s3_key)
                    s3_modified_datetime = s3_object.last_modified.replace(tzinfo=None)
                    if modified_datetime > s3_modified_datetime:
                        # Upload the newer file to S3
                        with sftp_conn.open(file_path, "rb") as body:
                            if not save_file_to_s3(bucket_name, s3_key, body, code_to_ascii(metadata.to_dict())):
                                return False
                except FileNotFoundError:
                    # Upload the file to S3 if it doesn't exist
                    with sftp_conn.open(file_path, "rb") as body:
                        if not save_file_to_s3(bucket_name, s3_key, body, code_to_ascii(metadata.to_dict())):
                            return False
    except Exception as e:
        send_email_task = EmailOperator(
            task_id='send_email_on_exception',
            to=dag_args["email"],
            subject='Exception in harvest_from_linux function',
            html_content=f'<p>An error occurred in the harvest_from_linux function: {str(e)}</p>',
            dag=dag,
        )
        send_email_task.execute(context=None)
        logging.error(f"Exception occurred: {str(e)}")


def harvest_files_to_s3():
    for iid in get_installed_instruments():  # iid[0] = instrumentId, iid[1] = instrumentAcronym
        metadata_df = get_measurement_metadata_by_instrument(iid[0], iid[1])
        logging.info(f"Metadata df: {metadata_df}")
        for _, metadata_series in metadata_df.iterrows():
            match metadata_series["instrument_custom_metadata"]["pc_os"]:
                case "Windows":
                    return harvest_from_windows(iid[0], metadata_series)
                case "Linux":
                    return harvest_from_linux(iid[0], metadata_series)
                case _:
                    return False

    return True


with DAG(dag_id="measurement_harvesting_daily", default_args=dag_args) as dag:
    copy_files_to_s3_task = PythonOperator(
        task_id="harvest_files_to_s3",
        python_callable=harvest_files_to_s3,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
