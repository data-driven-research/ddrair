import datetime
import logging

import pandas as pd
from airflow import DAG
from airflow.models import Variable
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago, timedelta
from pandas import DataFrame

from helpers.pandas_s3 import xls_to_df, df_to_xls
from helpers.cesnet_s3 import delete_file_from_s3
from helpers.postgres import get_instruments
from helpers.elastic import get_logbook_data
from helpers.datetime import hmsftime

dag_args = {
    "owner": "cxi_tul",
    "start_date": days_ago(1),
    "schedule_interval": "0 0 * * *",
    "email_on_failure": True,
    "email": ["jakub.zach@tul.cz", "ddr@tul.cz"]
}

RENAMED_COLS = {"measurement_acronym": "Název měření", "measurement_repetition": "Opakování",
                "resource_internal_number": "Interní číslo zdroje","resource_activity": "Činnost",
                "resource_acronym": "Akronym zdroje", "user_department": "Oddělení", "user_email": "Provedl"}
ORDERED_COLS = ["Název měření", "Opakování", "Datum a čas provedení", "Doba měření [h:m:s]", "Interní číslo zdroje",
                "Činnost", "Akronym zdroje", "Oddělení", "Provedl"]
DATETIME_PATTERN = "%Y-%m-%d %H:%M"


def transform_log_data(log_data_df: DataFrame):
    logging.info(f"Log data columns: {log_data_df.columns}")
    log_data_df["measurement_end"] = pd.to_datetime(log_data_df["measurement_end"])
    log_data_df["measurement_start"] = pd.to_datetime(log_data_df["measurement_start"])
    log_data_df["Datum a čas provedení"] = log_data_df["measurement_start"].dt.strftime(DATETIME_PATTERN)
    log_data_df["Doba měření [h:m:s]"] = ((log_data_df["measurement_end"] - log_data_df["measurement_start"])
                                          .apply(hmsftime))

    log_data_df.rename(columns=RENAMED_COLS, inplace=True)
    log_data_df = log_data_df[ORDERED_COLS]
    log_data_df = log_data_df.reindex(columns=ORDERED_COLS)
    log_data_df.sort_values(["Datum a čas provedení", "Opakování"], axis=0, inplace=True, ignore_index=True)

    return log_data_df


def export_logbook():
    instrument_ids = get_instruments()
    current_midnight = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    bucket_name = Variable.get("instrument_data_bucket_name", default_var="instrument-data")

    for iid in instrument_ids:  # iid[0] = instrumentId, iid[1] = instrumentAcronym
        previous_midnight = current_midnight - datetime.timedelta(days=1)
        previous_key = f"logs/{iid[0]}-{iid[1]}-{previous_midnight.date()}.xlsx"
        delete_old_logbook = False
        for _ in range(7):
            try:
                logbook = xls_to_df(bucket_name, previous_key)
                delete_old_logbook = True
                break
            except FileNotFoundError:
                previous_midnight = previous_midnight - datetime.timedelta(days=1)
        else:
            logbook = pd.DataFrame(columns=ORDERED_COLS)
            previous_midnight = previous_midnight.replace(month=1, day=1)

        log_data_df = get_logbook_data(iid[0], from_ts=previous_midnight, to_ts=current_midnight)
        if not log_data_df.empty:
            log_data_df = transform_log_data(log_data_df)
            logbook = pd.concat([logbook, log_data_df], axis=0, ignore_index=True)

        current_key = f"{iid[0]}-{iid[1]}-{current_midnight.date()}.xlsx"
        df_to_xls(logbook, bucket_name, current_key)

        if delete_old_logbook:
            delete_file_from_s3(bucket_name, previous_key)

    return True


with DAG(dag_id="instrument_logbook_export", default_args=dag_args) as dag:
    export_logbook_task = PythonOperator(
        task_id="export_logbook",
        python_callable=export_logbook,
        provide_context=True,
        retries=3,
        retry_delay=timedelta(seconds=15)
    )
