from typing import Sequence
import logging

from airflow.providers.samba.hooks.samba import SambaHook
from airflow.sensors.base import BaseSensorOperator

from helpers.samba import traverse_samba_directory


class SambaFileSensor(BaseSensorOperator):
    """
    Sensor to monitor a Samba share directory for new files.
    """
    template_fields: Sequence[str] = ("data_path", "samba_conn_id")

    def __init__(
            self,
            samba_conn_id,
            data_path,
            *args,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.samba_conn_id = samba_conn_id
        self.data_path = data_path

    def poke(self, context):
        """
        Poke the sensor to check for new files.
        """
        with SambaHook(samba_conn_id=self.samba_conn_id) as samba_hook:
            logging.info(f"Checking path {self.data_path}")
            file_list = traverse_samba_directory(samba_hook, self.data_path)
            logging.info(file_list)
        # Check if there are any files in the directory
        if not file_list:
            return False
        # Check if there are any new files in the directory
        known_files = context.get("task_instance", {}).xcom_pull(
            key="detected_files",
            task_ids=context["ti"].task_id,
            default=[]
        )
        new_files = [f for f in file_list if f not in known_files]
        if not new_files:
            return False
        # Set the new file list in the context for future runs
        context["ti"].xcom_push(key="detected_files", value=file_list)
        # Return True if there are new files in the directory
        return True
