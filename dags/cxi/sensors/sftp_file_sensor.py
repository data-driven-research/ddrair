from typing import Sequence
import logging

from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.sensors.base import BaseSensorOperator

from helpers.sftp import recursive_list_directory


class CustomSFTPSensor(BaseSensorOperator):
    """
    Sensor to monitor a SFTP directory for new files.
    """
    template_fields: Sequence[str] = ("data_path", "sftp_conn_id", "root_dir")

    def __init__(
            self,
            sftp_conn_id,
            root_dir,
            data_path,
            *args,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.sftp_conn_id = sftp_conn_id
        self.root_dir = root_dir
        self.data_path = data_path

    def poke(self, context):
        """
        Poke the sensor to check for new files.
        """
        sftp_hook = SFTPHook(ssh_conn_id=self.sftp_conn_id)
        with sftp_hook.get_conn() as sftp_conn:
            logging.info(f"Going to directory {self.root_dir}")
            sftp_conn.chdir(self.root_dir)
            logging.info(f"Checking path {self.data_path}")
            file_list = recursive_list_directory(sftp_conn, self.data_path)
            logging.info(file_list)
        # Check if there are any files in the directory
        if not file_list:
            return False
        # Check if there are any new files in the directory
        known_files = context.get("task_instance", {}).xcom_pull(
            key="detected_files",
            task_ids=context["ti"].task_id,
            default=[]
        )
        new_files = [f for f in file_list if f not in known_files]
        if not new_files:
            return False
        # Set the new file list in the context for future runs
        context["ti"].xcom_push(key="detected_files", value=file_list)
        # Return True if there are new files in the directory
        return True
