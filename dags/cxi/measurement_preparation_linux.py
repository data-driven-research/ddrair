import logging

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.utils.dates import days_ago, timedelta

from helpers.cesnet_s3 import create_s3_bucket


dag_args = {
    "owner": "cxi_tul",
    "start_date": days_ago(1),
    "email_on_failure": True,
    "email": ["jakub.zach@tul.cz", "ddr@tul.cz"]
}


def create_bucket(**kwargs):
    bucket_name = kwargs["dag_run"].conf["bucket_name"]
    return create_s3_bucket(bucket_name)


def create_pc_dirs(**kwargs):
    data_path = kwargs["dag_run"].conf["data_path"]
    root_dir = kwargs["dag_run"].conf["root_dir"]

    sftp_hook = SFTPHook(ssh_conn_id=kwargs["dag_run"].conf["instrument_id"])
    with sftp_hook.get_conn() as sftp_conn:
        logging.info(f"Going to directory {root_dir}")
        sftp_conn.chdir(root_dir)
        for directory in data_path.split("/"):
            if directory == "":
                continue
            if not sftp_hook.path_exists(directory):
                sftp_conn.mkdir(directory)
            sftp_conn.chdir(directory)

    return True


with DAG(dag_id="measurement_preparation_linux", default_args=dag_args, schedule=None) as dag:
    create_bucket_task = PythonOperator(
        task_id="create_bucket",
        python_callable=create_bucket,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
    create_pc_dirs_task = PythonOperator(
        task_id="create_pc_dirs",
        python_callable=create_pc_dirs,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
