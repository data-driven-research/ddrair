import logging

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.samba.hooks.samba import SambaHook
from airflow.utils.dates import days_ago, timedelta
from os import path

from sensors.samba_file_sensor import SambaFileSensor
from helpers.samba import traverse_samba_directory
from helpers.cesnet_s3 import save_file_to_s3
from helpers.elastic import get_measurement_metadata
from helpers.postgres import set_measurement_uploaded


dag_args = {
    "owner": "cxi_tul",
    "start_date": days_ago(1),
    "email_on_failure": True,
    "email": ["jakub.zach@tul.cz", "ddr@tul.cz"]
}


def move_bulk_files(**kwargs):
    data_path = kwargs["dag_run"].conf["data_path"]
    bulk_dir = kwargs["dag_run"].conf["bulk_dir"]
    if bulk_dir == "":
        return True

    with SambaHook(samba_conn_id=str(kwargs["dag_run"].conf["instrument_id"])) as samba_hook:
        for file_name in traverse_samba_directory(samba_hook, bulk_dir):
            src_file_path = file_name
            dst_file_path = path.join(data_path, path.basename(file_name))
            samba_hook.rename(src_file_path, dst_file_path)

    return True


def copy_files_to_s3(**kwargs):
    key_prefix = kwargs["dag_run"].conf["key_prefix"]
    bucket_name = kwargs["dag_run"].conf["bucket_name"]
    data_path = kwargs["dag_run"].conf["data_path"]
    measurement_id = kwargs["dag_run"].conf["measurement_id"]
    metadata = get_measurement_metadata(measurement_id)

    with SambaHook(samba_conn_id=str(kwargs["dag_run"].conf["instrument_id"])) as samba_hook:
        logging.info(f"Copying files from relpath {data_path}")
        file_list = traverse_samba_directory(samba_hook, data_path)

        for file_path in file_list:
            s3_key = path.join(key_prefix, file_path.replace(data_path, ""))
            with samba_hook.open_file(file_path, "rb") as body:
                if not save_file_to_s3(bucket_name, s3_key, body, metadata):
                    return False

    return True


def confirm_data_backup(**kwargs):
    measurement_id = kwargs["dag_run"].conf["measurement_id"]
    return set_measurement_uploaded(measurement_id)


with DAG(dag_id="measurement_backup_windows", default_args=dag_args, schedule=None) as dag:
    move_bulk_files_task = PythonOperator(
        task_id="move_bulk_files",
        python_callable=move_bulk_files,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
    sense_new_files_task = SambaFileSensor(
        task_id="sense_new_files",
        samba_conn_id="{{ dag_run.conf['instrument_id'] }}",
        data_path="{{ dag_run.conf['data_path'] }}",
        poke_interval=600,
        mode="reschedule",
        timeout=300

    )
    copy_files_to_s3_task = PythonOperator(
        task_id="copy_files_to_s3",
        python_callable=copy_files_to_s3,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
    confirm_data_backup_task = PythonOperator(
        task_id="confirm_data_backup",
        python_callable=confirm_data_backup,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )

    move_bulk_files_task >> sense_new_files_task >> copy_files_to_s3_task >> confirm_data_backup_task
