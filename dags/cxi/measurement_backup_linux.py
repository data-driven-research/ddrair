import logging

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.sftp.hooks.sftp import SFTPHook
from airflow.utils.dates import days_ago, timedelta
from os import path

from sensors.sftp_file_sensor import CustomSFTPSensor
from helpers.sftp import recursive_list_directory
from helpers.cesnet_s3 import save_file_to_s3
from helpers.elastic import get_measurement_metadata
from helpers.postgres import set_measurement_uploaded


dag_args = {
    "owner": "cxi_tul",
    "start_date": days_ago(1),
    "email_on_failure": True,
    "email": ["jakub.zach@tul.cz", "ddr@tul.cz"]
}


def move_bulk_files(**kwargs):

    bulk_dir = kwargs["dag_run"].conf["bulk_dir"]
    if bulk_dir == "":
        return True

    root_dir = kwargs["dag_run"].conf["root_dir"]
    data_path = path.join(kwargs["dag_run"].conf["bucket_name"], kwargs["dag_run"].conf["key_prefix"])

    sftp_hook = SFTPHook(ssh_conn_id=kwargs["dag_run"].conf["instrument_id"])
    with sftp_hook.get_conn() as sftp_conn:
        logging.info(f"Going to directory {root_dir}")
        sftp_conn.chdir(root_dir)
        for file_name in recursive_list_directory(sftp_conn, bulk_dir):
            src_file_path = file_name
            dst_file_path = path.join(data_path, path.basename(file_name))
            sftp_conn.rename(src_file_path, dst_file_path)

    return True


def copy_files_to_s3(**kwargs):
    key_prefix = kwargs["dag_run"].conf["key_prefix"]
    bucket_name = kwargs["dag_run"].conf["bucket_name"]
    root_dir = kwargs["dag_run"].conf["root_dir"]
    data_path = kwargs["dag_run"].conf["data_path"]
    measurement_id = kwargs["dag_run"].conf["measurement_id"]
    metadata = get_measurement_metadata(measurement_id)

    sftp_hook = SFTPHook(ssh_conn_id=kwargs["dag_run"].conf["instrument_id"])
    with sftp_hook.get_conn() as sftp_conn:
        logging.info(f"Going to directory {root_dir}")
        sftp_conn.chdir(root_dir)
        logging.info(f"Copying files from relpath {data_path}")
        file_list = recursive_list_directory(sftp_conn, data_path)

        for file_path in file_list:
            s3_key = path.join(key_prefix, file_path.replace(data_path, ""))
            with sftp_conn.open(file_path, "rb") as body:
                if not save_file_to_s3(bucket_name, s3_key, body, metadata):
                    return False

    return True


def confirm_data_backup(**kwargs):
    measurement_id = kwargs["dag_run"].conf["measurement_id"]
    return set_measurement_uploaded(measurement_id)


with DAG(dag_id="measurement_backup_linux", default_args=dag_args, schedule=None) as dag:
    move_bulk_files_task = PythonOperator(
        task_id="move_bulk_files",
        python_callable=move_bulk_files,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
    sense_new_files_task = CustomSFTPSensor(
        task_id="sense_new_files",
        sftp_conn_id="{{ dag_run.conf['instrument_id'] }}",
        root_dir="{{ dag_run.conf['root_dir'] }}",
        data_path="{{ dag_run.conf['data_path'] }}",
        poke_interval=600,
        mode='reschedule',
        timeout=300
    )
    copy_files_to_s3_task = PythonOperator(
        task_id="copy_files_to_s3",
        python_callable=copy_files_to_s3,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )
    confirm_data_backup_task = PythonOperator(
        task_id="confirm_data_backup",
        python_callable=confirm_data_backup,
        provide_context=True,
        retries=1,
        retry_delay=timedelta(seconds=15)
    )

    move_bulk_files_task >> sense_new_files_task >> copy_files_to_s3_task >> confirm_data_backup_task
