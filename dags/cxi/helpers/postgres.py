from airflow.providers.postgres.hooks.postgres import PostgresHook

get_instruments_query = """
SELECT i.id, i.acronym
FROM instruments i;
"""

get_installed_instruments_query = """
SELECT i.id, i.acronym
FROM instruments i
WHERE i.installed = True;
"""

set_measurement_uploaded_query = """
UPDATE measurements
SET uploaded = True
WHERE id = %s
"""


def get_instruments():
    postgres_hook = PostgresHook(postgres_conn_id="postgres_conn")
    with postgres_hook.get_conn() as conn:
        with conn.cursor() as cursor:
            cursor.execute(get_instruments_query)
            result = cursor.fetchall()

    return result


def get_installed_instruments():
    postgres_hook = PostgresHook(postgres_conn_id="postgres_conn")
    with postgres_hook.get_conn() as conn:
        with conn.cursor() as cursor:
            cursor.execute(get_installed_instruments_query)
            result = cursor.fetchall()

    return result


def set_measurement_uploaded(measurement_id: str):
    postgres_hook = PostgresHook(postgres_conn_id="postgres_conn")
    with postgres_hook.get_conn() as conn:
        with conn.cursor() as curs:
            curs.execute(set_measurement_uploaded_query, (measurement_id, ))

    return True
