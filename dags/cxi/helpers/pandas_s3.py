from os import getenv

import pandas as pd
from pandas import DataFrame

# s3fs dependency for pandas

# Requires setting up environment variables
S3_ENDPOINT_URL = getenv("S3_ENDPOINT_URL")
S3_ACCESS_KEY_ID = getenv("S3_ACCESS_KEY_ID")
S3_SECRET_ACCESS_KEY = getenv("S3_SECRET_ACCESS_KEY")


def df_to_csv(df: DataFrame, bucket: str, key: str, index=False):
    """
    Helper function to save DataFrame directly to CESNET S3
    :param index: if save index as well
    :param df: DataFrame to save
    :param bucket: object's bucket where to save
    :param key: key without .csv
    """
    df.to_csv(f"s3://{bucket}/{key}",
              index=index,
              storage_options={
                  "client_kwargs": {"endpoint_url": S3_ENDPOINT_URL},
                  "key": S3_ACCESS_KEY_ID,
                  "secret": S3_SECRET_ACCESS_KEY
              })


def get_formats(workbook):
    format_left_ = workbook.add_format()
    format_left_.set_align("left")
    format_center_ = workbook.add_format()
    format_center_.set_align("center")
    format_right_ = workbook.add_format()
    format_right_.set_align("right")

    return format_left_, format_center_, format_right_


def df_to_xls(df: DataFrame, bucket: str, key: str, index=False):
    """
    Helper function to save DataFrame directly to CESNET S3
    :param index: if save index as well
    :param df: DataFrame to save
    :param bucket: object's bucket where to save
    :param key: key without .csv
    """
    writer = pd.ExcelWriter(f"s3://{bucket}/{key}",
                            engine="xlsxwriter",
                            storage_options={
                                "client_kwargs": {"endpoint_url": S3_ENDPOINT_URL},
                                "key": S3_ACCESS_KEY_ID,
                                "secret": S3_SECRET_ACCESS_KEY
                            })
    df.to_excel(writer, sheet_name="logbook", index=index)
    workbook = writer.book
    worksheet = writer.sheets["logbook"]
    format_left, format_center, format_right = get_formats(workbook)

    for idx, col in enumerate(df):
        series = df[col]
        max_len = max((
            series.astype(str).map(len).max(),  # len of largest item
            len(str(series.name))  # len of column name/header
        ))
        format_ = format_center
        if idx == 0:
            format_ = format_left
        elif idx == (len(df)-1):
            format_ = format_right
        worksheet.set_column(idx, idx, max_len, format_)
    writer.close()


def csv_to_df(bucket: str, key: str, header=0):
    """
        Helper function to load DataFrame directly from CESNET S3
        :param header: which row to read as header
        :param bucket: object's bucket from where to load
        :param key: object key without .csv
        """
    return pd.read_csv(f"s3://{bucket}/{key}",
                       header=header,
                       storage_options={
                           "client_kwargs": {"endpoint_url": S3_ENDPOINT_URL},
                           "key": S3_ACCESS_KEY_ID,
                           "secret": S3_SECRET_ACCESS_KEY
                       })


def xls_to_df(bucket: str, key: str, header=0):
    """
        Helper function to load DataFrame directly from CESNET S3
        :param header: which row to read as header
        :param bucket: object's bucket from where to load
        :param key: object key without .csv
        """
    return pd.read_excel(f"s3://{bucket}/{key}",
                         header=header,
                         storage_options={
                             "client_kwargs": {"endpoint_url": S3_ENDPOINT_URL},
                             "key": S3_ACCESS_KEY_ID,
                             "secret": S3_SECRET_ACCESS_KEY
                         })
