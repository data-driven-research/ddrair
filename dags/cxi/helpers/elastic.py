from os import environ
import logging
from datetime import datetime

from airflow.providers.elasticsearch.hooks.elasticsearch import ElasticsearchPythonHook
import pandas as pd

from helpers.metadata import code_to_ascii

DATETIME_PATTERN = "%Y-%m-%dT%H:%M:%S"


def __get_hook__():
    es_hosts = [environ.get("ES_HOST")]
    es_conn_args = {"api_key": (environ.get("ES_API_ID"), environ.get("ES_API_KEY")),
                    "ca_certs": "/opt/airflow/certs/ca.crt"}
    return ElasticsearchPythonHook(hosts=es_hosts, es_conn_args=es_conn_args)


def __preprocess_search_result__(result: dict, cols=None):
    results = pd.DataFrame(columns=cols)
    for hit in result["hits"]:
        results = pd.concat([results, pd.DataFrame([hit["_source"]])], ignore_index=True)

    return results


def get_measurement_metadata(measurement_id: str):
    logging.info(f"Getting metadata about measurement {measurement_id}")
    es_hook = __get_hook__()
    query = {
        "query": {
            "bool": {
                "should": [
                    {
                        "match": {
                            "measurement_id": f"{measurement_id}"}}
                ],
                "minimum_should_match": 1
            }
        },
        "_source": {
            "excludes": ["id", "_class"]
        }
    }
    logging.info(f"ES query: {query}")
    result = es_hook.search(query=query, index="measurement_metadata")
    source = result["hits"][0]["_source"]
    metadata = code_to_ascii(source)

    return metadata


def get_measurement_metadata_by_instrument(instrument_id: str, instrument_acronym: str):
    logging.info(f"Getting measurement metadata of instrument {instrument_id}")
    es_hook = __get_hook__()
    query = {
        "query": {
            "bool": {
                "should": [
                    {
                        "match": {
                            "instrument_id": f"{instrument_id}"}
                    },
                    {
                        "match": {
                            "instrument_acronym": f"{instrument_acronym}"
                        }
                    }
                ]
            }
        },
        "_source": {
            "excludes": ["id", "_class"]
        }
    }
    logging.info(f"ES query: {query}")
    result = es_hook.search(query=query, index="measurement_metadata")

    return __preprocess_search_result__(result)


def get_logbook_data(instrument_id: str, from_ts: datetime, to_ts: datetime):
    logging.info(f"Getting measurement metadata of instrument {instrument_id}")
    logbook_cols = ["measurement_acronym", "measurement_repetition",  "measurement_start", "measurement_end", "resource_internal_number",
                    "resource_activity", "resource_acronym", "user_department", "user_email"]
    es_hook = __get_hook__()
    query = {
        "query": {
            "bool": {
                "should": [
                    {
                        "match": {
                            "instrument_id": f"{instrument_id}"}},
                    {
                        "range": {
                            "measurement_end": {
                                "time_zone": "Europe/Prague",
                                "gte": f"{from_ts.strftime(DATETIME_PATTERN)}",
                                "lt": f"{to_ts.strftime(DATETIME_PATTERN)}"
                            }
                        }
                    }
                ]
            }
        },
        "_source": logbook_cols
    }
    logging.info(f"ES query: {query}")
    result = es_hook.search(query=query, index="measurement_metadata")

    return __preprocess_search_result__(result, cols=logbook_cols)
