from os import path

from airflow.providers.samba.hooks.samba import SambaHook


def traverse_samba_directory(samba_hook: SambaHook, samba_path: str):
    file_list = []

    for root, _, files in samba_hook.walk(samba_path):
        for file in files:
            file_list.append(file)

    return file_list
