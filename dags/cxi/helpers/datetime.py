from datetime import timedelta


def hmsftime(td: timedelta):
    days = td.days
    hours, remainder = divmod(td.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    hours += days * 24

    return f"{hours:02d}:{minutes:02d}:{seconds:02d}"
