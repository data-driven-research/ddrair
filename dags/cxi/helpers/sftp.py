import logging
from _stat import S_ISDIR, S_ISREG
from os import path

from paramiko import SFTPClient


def recursive_list_directory(sftp_conn: SFTPClient, sftp_path: str):
    file_list = []

    for entry in sftp_conn.listdir_attr(sftp_path):
        remote_path = path.join(sftp_path, entry.filename)
        logging.info(f"Current sftp_path {remote_path}")
        mode = entry.st_mode
        if S_ISDIR(mode):
            file_list.extend(recursive_list_directory(sftp_conn, remote_path))
        elif S_ISREG(mode):
            file_list.append(remote_path)

    return file_list
