from os import environ
import logging

from boto3 import session


def __get_session__():
    s3_session = session.Session(
        aws_access_key_id=environ.get("S3_ACCESS_KEY_ID"),
        aws_secret_access_key=environ.get("S3_SECRET_ACCESS_KEY"),
        region_name="storage"
    )

    return s3_session.resource("s3", endpoint_url=environ.get("S3_ENDPOINT_URL"))


def save_file_to_s3(bucket_name: str, key: str, body, metadata: dict):
    s3 = __get_session__()
    bucket = s3.Bucket(bucket_name)
    logging.info(f"Saving object {key}")
    bucket.put_object(Key=key, Body=body, Metadata=metadata)

    return True


def delete_file_from_s3(bucket_name: str, key: str):
    s3 = __get_session__()
    bucket = s3.Bucket(bucket_name)
    logging.info(f"Deleting object {key}")
    bucket.delete_objects(Delete={
        "Objects": [
            {
                "Key": key,
            },
        ],
        "Quiet": True
    })

    return True


def get_known_s3_files(bucket_name: str, key_prefix: str):
    s3 = __get_session__()
    bucket = s3.Bucket(bucket_name)
    logging.info(f"Checking files in bucket {bucket_name}")
    known_files = [obj for obj in bucket.objects.filter(Prefix=key_prefix)]

    return known_files


def create_s3_bucket(bucket_name: str):
    s3 = __get_session__()
    logging.info(f"Creating bucket {bucket_name}")
    s3.create_bucket(Bucket=bucket_name)

    return True


def get_object_on_s3(bucket_name: str, key: str):
    s3 = __get_session__()
    logging.info(f"Getting object {bucket_name}/{key}")
    return s3.Object(bucket_name, key)

