import unicodedata


def code_to_ascii(source: dict):
    return {k: unicodedata.normalize("NFD", str(v)).encode("ascii", "ignore").decode("ascii") for k, v in
            source.items()}
